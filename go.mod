module github.com/motemen/gore

go 1.12

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/motemen/go-quickfix v0.0.0-20160413151302-5c522febc679
	github.com/peterh/liner v1.1.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/text v0.3.2
	golang.org/x/tools v0.0.0-20190717194535-128ec6dfca09
)
